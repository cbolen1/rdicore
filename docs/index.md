RDI
-------------------------------------------------------------------------------

The RDI package is part of the [Immcantation](http://immcantation.readthedocs.io) 
analysis framework for Adaptive Immune Receptor Repertoire sequencing 
(AIRR-seq) and provides an methods for visualizing and calculating the 
Repertoire Dissimilarity Index (RDI).

The Repertoire Dissimilarity Index is a non-parametric method for directly 
comparing sequencing repertoires, with the goal of rigorously quantifying 
differences in V, D, and J gene segment utilization. This method uses a 
bootstrapped subsampling approach to account for variance in sequencing depth, 
and, coupled with a data simulation approach, allows for direct quantification 
of the average variation between repertoires.

Contact
-------------------------------------------------------------------------------

For help and questions please contact the [Immcantation Group](mailto:immcantation@googlegroups.com)
or use the [issue tracker](https://bitbucket.org/cbolen1/rdicore/issues?status=new&status=open).


# Dependencies

**Imports:** beanplot, gplots, pdist, stringr  
**Suggests:** knitr, ggplot2


# Authors

[Christopher Bolen](mailto:cbolen1@gmail.com) (aut, cre)  
[Florian Rubelt](mailto:frubelt@stanford.edu) (aut)  
[Jason Vander Heiden](mailto:jason.vanderheiden@yale.edu) (aut)


# Citing


To cite the RDI method in publications use:

  Bolen CR, Rubelt F, Vander Heiden JA, Davis MM. The Repertoire Dissimilarity Index as a method to compare lymphocyte
  receptor repertoires. BMC Bioinformatics. 2017 Mar 7;18(1):155.

A BibTeX entry for LaTeX users is

  @Article{,
    title = {The Repertoire Dissimilarity Index as a method to compare lymphocyte receptor repertoires},
    author = {Christopher R Bolen and Florian Rubelt and Jason A {Vander Heiden} and Mark M Davis},
    journal = {BMC Bioinformatics},
    year = {2017},
    volume = {18},
    pages = {155},
  }

