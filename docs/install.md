Download
-------------------------------------------------------------------------------

The latest stable release of RDI can be downloaded from 
[CRAN](http://cran.rstudio.com/web/packages/rdi).

Installing Released Versions
-------------------------------------------------------------------------------

The simplest way to install RDI is via CRAN:

```R
install.packages("rdi")
```

Building Development Versions
-------------------------------------------------------------------------------

To build from the [source code](http://bitbucket.org/cbolen1/rdicore),
first install the build dependencies:

```R
install.packages(c("devtools", "roxygen2", "testthat", "knitr", "rmarkdown"))
```

To install the latest development code via devtools:

```R
library(devtools)
install_bitbucket("cbolen1/rdicore@production")
```

Note, using `install_bitbucket` will not build the documentation. To generate the 
documentation, clone the repository and build as normal using devtools, 
roxygen and knitr:

```R
library(devtools)
install_deps()
document()
build()
install()
```
