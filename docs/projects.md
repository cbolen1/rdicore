Related Projects
-------------------------------------------------------------------------------

* [Immcantation Portal](http://immcantation.readthedocs.io) - 
  Overview of related projects
* [pRESTO](http://presto.readthedocs.io) - 
  Raw read assembly, quality control and UMI processing 
* [Change-O](http://changeo.readthedocs.io) - 
  V(D)J alignment standardization and clonal clustering
* [Alakazam](http://alakazam.readthedocs.io) - 
  Lineage reconstruction, V(D)J gene usage, repertoire diversity and 
  physicochemical property analysis
* [SHazaM](http://shazam.readthedocs.io) - 
  Mutation profiling and selection strength quantification
  physicochemical property analysis
* [TIgGER](http://tigger.readthedocs.io) - 
  Polymorphism detection and genotyping
