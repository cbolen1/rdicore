**plotRDIladder** - *RDI ladder plotting function*

Description
--------------------

function for adding a pre-computed RDI ladder onto a plot


Usage
--------------------
```
plotRDIladder(ladder, side = 4, toPlot = NULL, labelLadder = TRUE,
add = TRUE, cex = 0.7, lineCol = NULL, fillCol = "#AAAAAA")
```

Arguments
-------------------

ladder
:   the ladder object to add, as created by [rdiLadder](rdiLadder.md)

side
:   integer; value between 1 and 4 indicating where the ladder will be 
added. 1 - bottom, 2 - left, 3 - top, 4 - right.

toPlot
:   logical vector; which ladders should be plotted? By default, 
ladders that are significantly overlapped by their neighbor and
those that are majority outside the plotting region are removed.

labelLadder
:   logical; if `TRUE`, each curve will be annotated with the ladder name

add
:   logical; if `TRUE`, the ladder will be added to the current plot

cex
:   character expansion for ladder labels.

lineCol
:   the colors to be used for the ladder border. If the length of `col` 
is less than the length of ladder, `col` will be recycled.

fillCol
:   the colors to be used to fill the ladder. If the length of `col` 
is less than the length of ladder, `col` will be recycled.




Value
-------------------

Invisibly returns the location of the ladder (if side 1 or 3, the y location; 
otherwise, the x location).


Details
-------------------

This function is used in conjunction with [rdiLadder](rdiLadder.md) to add a useful 
annotation to any plot containing RDI values. 

Because RDI values vary according to the number of genes and size of the repertoires,
they are not useful as numbers by themselves. Instead, it is useful to compare them 
with estimates of the true difference between the two repertoires. This function
adds a series of density curves along one side of a standard plotting region, each
one representing the most likely RDI values between two repertoires that vary by a set
amount. 

By default, not all density curves from the `ladder` parameter are plotted. 
Instead, the function intelligently chooses which ladders to plot based on the amount
of overlap between neighboring ladders. If a ladder is significantly overlapped by the
ladder below it, then the ladder will not be plotted. In addition, if the mean of a 
ladder is outside the main plotting region, it will be dropped. In order to control this 
behavior, you can directly specify which ladders are plotted using the `toPlot` 
parameter.




See also
-------------------

[rdiLadder](rdiLadder.md), [rdiModel](rdiModel.md), [rdiAxis](rdiAxis.md)



